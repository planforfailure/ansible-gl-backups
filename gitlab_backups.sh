#!/bin/bash

cfg=$wd/ansible.cfg
wd=/home/user/scm-backup
inv=/home/user/inventory
out=/tmp/scm_`date '+%d-%m-%y'`.out
final="`grep -F -A1 '[scm]' /home/user/inventory | grep -v scm | awk '{ print $1 }'`"
 
  cd $wd
  printf '\n'  
  echo -e "\e[34mgitlab host to be backed up:-\e[0m"
  printf "$final"
  printf '\n' 
  sleep 3 
  /usr/bin/ansible-playbook --extra-vars '@passwd.yml' gitlab_backups.yml  > $out
